# Integrated Collaborative Map Manipulation Tool Set

This repository is also the home of the ICMMTS, Integrated
Collaborative Map Manipulation Tool Set, a set of scripts to do stuff
with maps. Note that this does not handle generation, instead use
[this map generator](https://github.com/Azgaar/Fantasy-Map-Generator)
and import the map data.

## Create topography

First, generate your map with FMG. Then, open the dev console and copy
and run the script inside `extractor.js`. This should give you a file
`pack.json`.

Now, create a directory for your map data, move there, and run
`import_pack.py < path/to/pack.json`. This will produce a
`pack.json.gz` file in the current directory.

Now, copy `params.json` in the ICMMTS base to the current directory
and fill in the details based on stats FMG gives you. Congratulations,
you now have a full map!

## Add political regions

Create a `.txt` file with the name of the nation, with the following
format:

    {
        "name": name,
        "colour": "rgb(r, g, b)"
    }
    %%
    cell 1
    cell 2
    ...

Where there is a cell ID on each line. Comments are also allowed
within the file with `#`.

## Render the map

Use `render.py` in the directory to output an SVG. Options of the form
`a=b` are supported:

- `color={height|biome|nation} [height]` - Coloring method.
- `select=all` - Cell selection.
- `width=N [1000]` - Width of output SVG.
- `height=N [500]` - Height of output SVG.
- `rivers=B [1]` - Whether to show rivers.
- `legend=B [1]` - Whether to show the legend.
- `show_id=B []` - Whether to show cell IDs.
- `nation_legends=B [1]` - Whether to show nation names.

## Validate claims

The `validate.py` script checks if there are overlapping claims or
claims on water tiles and reports them.
