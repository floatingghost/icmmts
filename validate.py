#!/usr/bin/env python3

import libicmmts

pack = libicmmts.get_pack()
nations = libicmmts.get_nations()

blocks = {}
for k, n in nations.items():
    for b in n[1]:
        if b not in blocks:
            blocks[b] = set()
        blocks[b].add(k)

err = 0
for b, v in blocks.items():
    f = str(pack['cells']['feature'][b])
    if not pack['features'][f]['land']:
        err += 1
        print('%d: water claims: %s' % (b, ', '.join(v)))
    elif len(v) > 1:
        err += 1
        print('%d: overlapping claims: %s' % (b, ', '.join(v)))

nations_with_colours = [x for x in nations.values() if "colour" in x[0]]
for n in nations_with_colours:
    colour_overlaps = [
        x for x in nations_with_colours if x[0]["colour"] == n[0]["colour"]
    ]
    if len(colour_overlaps) != 1:
        err += 1
        print("%s: duplicated colour %s in %s" % (
            n[0]["name"], n[0]["colour"],
            ", ".join(x[0]["name"] for x in colour_overlaps)
        ))

print('%d errors' % err)
if err > 0:
    exit(1)
