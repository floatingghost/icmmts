import gzip
import json
import os

def get_pack():
    with gzip.open('pack.json.gz', 'rt') as f:
        return json.load(f)

def get_params():
    with open('params.json', 'r') as f:
        return json.load(f)

def get_nations():
    files = []
    for x in os.listdir('.'):
        if x.endswith('.txt'):
            files.append(x)
    files.sort()
    nations = {}
    for x in files:
        i = x[:-4]
        obj = ''
        cells = set()
        mode = 0
        with open(x, 'r') as f:
            for l in f:
                ls = l.strip()
                if not ls or l[0] == '#':
                    continue
                if mode == 0:
                    if l == '%%\n':
                        mode = 1
                    else:
                        obj += l
                else:
                    cells.add(int(ls))
        nations[i] = (json.loads(obj), cells)
    return nations
