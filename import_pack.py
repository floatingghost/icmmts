#!/usr/bin/env python3

import gzip
import json
import sys

ipack = json.load(sys.stdin)

cell_fields = (
    ('coords', 'p'),
    ('neighbors', 'c'),
    ('vertices', 'v'),
    ('border', 'b'),
    ('height', 'h'),
    ('feature', 'f'),
    ('biome', 'biome'),
    ('river', 'r'),
)

pack = {}

pack['cells'] = {
    'num': len(ipack['cells']['p']),
}

for k, v in cell_fields:
    if isinstance(ipack['cells'][v], list):
        pack['cells'][k] = ipack['cells'][v]
    else:
        pack['cells'][k] = [0] * pack['cells']['num']
        for i in range(pack['cells']['num']):
            pack['cells'][k][i] = ipack['cells'][v][str(i)]

pack['vertices'] = ipack['vertices']['p']

pack['features'] = {}
for x in ipack['features'][1:]:
    pack['features'][x['i']] = x
    del pack['features'][x['i']]['i']

pack['rivers'] = {}
for x in ipack['rivers'][1:]:
    pack['rivers'][x['i']] = x
    del pack['rivers'][x['i']]['name']
    del pack['rivers'][x['i']]['i']

with gzip.open('pack.json.gz', 'wt') as f:
    json.dump(pack, f)
